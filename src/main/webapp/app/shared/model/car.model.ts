export interface ICar {
  id?: number;
  name?: string;
  type?: string;
  color?: string;
}

export class Car implements ICar {
  constructor(public id?: number, public name?: string, public type?: string, public color?: string) {}
}
